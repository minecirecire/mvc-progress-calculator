function doThing() {
    var bar = document.getElementById("theBar");
    if (width >= 100) { width = 1; setTimeout(function(){doThing();}, 10); } else { width++; bar.style.width = width + '%'; setTimeout(function(){doThing();}, 10); }
}

var partSelector = document.getElementById("partSelect");
console.log(partSelector);
console.log(partSelector.options[partSelector.selectedIndex]);

function hideAllSessions() {
    document.getElementById("sessionSelect1A").style.display = "none";
    document.getElementById("sessionSelect1B").style.display = "none";
    document.getElementById("sessionSelect1C").style.display = "none";
    document.getElementById("sessionSelect1E").style.display = "none";
    document.getElementById("sessionSelect2A").style.display = "none";
    document.getElementById("sessionSelect2B").style.display = "none";
    document.getElementById("sessionSelect2C").style.display = "none";
    document.getElementById("sessionSelect2E").style.display = "none";
    document.getElementById("sessionSelect3A").style.display = "none";
    document.getElementById("sessionSelect3B").style.display = "none";
    document.getElementById("sessionSelect3C").style.display = "none";
    document.getElementById("sessionSelect3E").style.display = "none";
    document.getElementById("sessionSelect4A").style.display = "none";
    document.getElementById("sessionSelect4B").style.display = "none";
    document.getElementById("sessionSelect4C").style.display = "none";
    document.getElementById("sessionSelect4E").style.display = "none";
    document.getElementById("sessionSelect4P").style.display = "none";
    document.getElementById("sessionSelectF").style.display = "none";
}

var progress = 0;
partSelector.addEventListener("change", function() {
    var curPart = partSelector.options[partSelector.selectedIndex].text;
    console.log(curPart);
    hideAllSessions();
    if (curPart == "Part 1-A") {
        document.getElementById("sessionSelect1A").style.display = "inline-block";
    }
    else if (curPart == "Part 1-B") {
        document.getElementById("sessionSelect1B").style.display = "inline-block";
    }
    else if (curPart == "Part 1-C") {
        document.getElementById("sessionSelect1C").style.display = "inline-block";
    }
    else if (curPart == "Exam 1") {
        document.getElementById("sessionSelect1E").style.display = "inline-block";
    }
    else if (curPart == "Part 2-A") {
        document.getElementById("sessionSelect2A").style.display = "inline-block";
    }
    else if (curPart == "Part 2-B") {
        document.getElementById("sessionSelect2B").style.display = "inline-block";
    }
    else if (curPart == "Part 2-C") {
        document.getElementById("sessionSelect2C").style.display = "inline-block";
    }
    else if (curPart == "Exam 2") {
        document.getElementById("sessionSelect2E").style.display = "inline-block";
    }
    else if (curPart == "Part 3-A") {
        document.getElementById("sessionSelect3A").style.display = "inline-block";
    }
    else if (curPart == "Part 3-B") {
        document.getElementById("sessionSelect3B").style.display = "inline-block";
    }
    else if (curPart == "Part 3-C") {
        document.getElementById("sessionSelect3C").style.display = "inline-block";
    }
    else if (curPart == "Exam 3") {
        document.getElementById("sessionSelect3E").style.display = "inline-block";
    }
    else if (curPart == "Part 4-A") {
        document.getElementById("sessionSelect4A").style.display = "inline-block";
    }
    else if (curPart == "Part 4-B") {
        document.getElementById("sessionSelect4B").style.display = "inline-block";
    }
    else if (curPart == "Part 4-C") {
        document.getElementById("sessionSelect4C").style.display = "inline-block";
    }
    else if (curPart == "Exam 4") {
        document.getElementById("sessionSelect4E").style.display = "inline-block";
    }
    else if (curPart == "Part 4-P") {
        document.getElementById("sessionSelect4P").style.display = "inline-block";
    }
    else if (curPart == "Final") {
        document.getElementById("sessionSelectF").style.display = "inline-block";
    }
});

function updateBar(progress) {
    var bar = document.getElementById("theBar");
    bar.style.width = 100*(progress/119) + '%';
    bar.innerHTML = Math.round(100*100*(progress/119))/100 + "%";
    updateProg();
}

document.getElementById("sessionSelect1A").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect1B").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect1C").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect1E").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect2A").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect2B").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect2C").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect2E").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect3A").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect3B").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect3C").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect3E").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect4A").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect4B").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect4C").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect4E").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelect4P").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });
document.getElementById("sessionSelectF").addEventListener("change", function(evt) { progress = evt.target.value; console.log(progress); updateBar(progress); });

function updateProg() {
    var curDay = document.getElementById("dayText").value;
    console.log(curDay);
    var bar = document.getElementById("idealProgress");
    bar.style.width = 100*(curDay/180) + "%";
    var idealProg = 100*(curDay/180);
    bar.innerHTML = Math.round(100*100*(curDay/180))/100 + "%";
    if ( progress != 0 ) {
        var aheadPercent = 100*(((100*(progress/119))-idealProg)/idealProg);
        aheadPercent = Math.round(100*aheadPercent)/100
        var aheadCounter = document.getElementById("percentageCounter");
        aheadCounter.innerHTML = "Aheadness Percent: " + aheadPercent + "%";
        console.log(aheadPercent); console.log(progress);
        if ( aheadPercent > 0 ) {
            aheadCounter.style = "color: green;display: inline-block";
        } else { aheadCounter.style = "color: red;display: inline-block"; }
    }
}


//document.getElementById("recalcIdeal").addEventListener("click", function() {
//    var curDay = document.getElementById("dayText").value;
//    console.log(curDay);
//    var bar = document.getElementById("idealProgress");
//    bar.style.width = 100*(curDay/180) + "%";
//    var idealProg = 100*(curDay/180);
//    bar.innerHTML = Math.round(100*100*(curDay/180))/100 + "%";
//    if ( progress != 0 ) {
//        var aheadPercent = 100*(((100*(progress/119))-idealProg)/idealProg);
//        aheadPercent = Math.round(100*aheadPercent)/100
//        var aheadCounter = document.getElementById("percentageCounter");
//        aheadCounter.innerHTML = "Aheadness Percent: " + aheadPercent + "%";
//        console.log(aheadPercent); console.log(progress);
//        if ( aheadPercent > 0 ) {
//            aheadCounter.style = "color: green";
//        } else { aheadCounter.style = "color: red"; }
//    }
//});

document.getElementById("dayText").addEventListener("change", function() {
    var curDay = document.getElementById("dayText").value;
    console.log(curDay);
    var bar = document.getElementById("idealProgress");
    bar.style.width = 100*(curDay/180) + "%";
    var idealProg = 100*(curDay/180);
    bar.innerHTML = Math.round(100*100*(curDay/180))/100 + "%";
    if ( progress != 0 ) {
        var aheadPercent = 100*(((100*(progress/119))-idealProg)/idealProg);
        aheadPercent = Math.round(100*aheadPercent)/100
        var aheadCounter = document.getElementById("percentageCounter");
        aheadCounter.innerHTML = "Aheadness Percent: " + aheadPercent + "%";
        console.log(aheadPercent); console.log(progress);
        if ( aheadPercent > 0 ) {
            aheadCounter.style = "color: green;display: inline-block";
        } else { aheadCounter.style = "color: red;display: inline-block"; }
    }
});